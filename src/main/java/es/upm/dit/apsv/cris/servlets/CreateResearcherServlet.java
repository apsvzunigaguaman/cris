package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

/**
 * Servlet implementation class CreateResearcherServlet
 */
@WebServlet("/CreateResearcherServlet")
public class CreateResearcherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Researcher r = new Researcher();
		r.setId((String) request.getParameter("id"));
		r.setName((String) request.getParameter("name"));
		r.setLastname((String) request.getParameter("lastname"));
		r.setEmail((String) request.getParameter("email"));
		r.setScopusURL((String) request.getParameter("scopusURL"));
		r.setPassword((String) request.getParameter("password"));
		
		Client client = ClientBuilder.newClient(new ClientConfig());
		client.target("http://localhost:8080/CRISSERVICE/rest/Researchers/"
				).request().post(Entity.entity(r, MediaType.APPLICATION_JSON));
		response.sendRedirect(request.getContextPath() + "/AdminServlet");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
